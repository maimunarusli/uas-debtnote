import React from 'react'
import { View, Text,TouchableOpacity,TextInput } from 'react-native'

const KontenLogin = () => {
    return (
        <View style={{height:'45%',paddingTop:30, backgroundColor:'white',marginHorizontal:40,justifyContent:'center'}}>
                <View style={{height:30,borderBottomColor:'#BCFFB9',paddingBottom:10,borderWidth:2,borderColor:'white',marginHorizontal:91}}>
                    <Text style={{textAlign:'center'}}>DEBTNOTE</Text>
                </View>
                <TextInput placeholder='Email' style={{height:50,borderBottomColor:'#BCFFB9',marginHorizontal:40,borderWidth:2,borderColor:'white'}}/>
                <TextInput placeholder='Password' style={{height:50,borderBottomColor:'#BCFFB9',marginHorizontal:40,borderWidth:2,borderColor:'white'}}/>
            <TouchableOpacity style={{backgroundColor:'#BCFFB9',marginHorizontal:88,height:'37%',justifyContent:'center',borderRadius:50,marginTop:20}}>
                <Text style={{textAlign:'center',textTransform:'uppercase',fontWeight:'bold',fontSize:13}}>LOGIN</Text>
            </TouchableOpacity>
            </View> 
    )
}

export default KontenLogin;