import Home from './Home'
import TambahKontak from './TambahKontak'
import DetailKontak from './DetailKontak'
import EditKontak from './EditKontak'
import login from './login/login'

export {Home, TambahKontak, DetailKontak, EditKontak,login}