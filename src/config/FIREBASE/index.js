import firebase from 'firebase'

firebase.initializeApp({
  apiKey: "AIzaSyBGXk5ASp6Hmjs1_q6ZchQQwe2jenndf4I",
  authDomain: "debtnote-40a75.firebaseapp.com",
  databaseURL: "https://debtnote-40a75-default-rtdb.firebaseio.com",
  projectId: "debtnote-40a75",
  storageBucket: "debtnote-40a75.appspot.com",
  messagingSenderId: "461826097891",
  appId: "1:461826097891:web:9b91b3c2b397e90d12bbd6",
  measurementId: "G-YNT0RG90V1"
})

const FIREBASE = firebase;

export default FIREBASE;